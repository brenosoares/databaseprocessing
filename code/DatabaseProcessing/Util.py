import pandas as pd
import numpy as np

from sklearn import preprocessing
from pandas import Series
from sklearn.preprocessing import MinMaxScaler

def normalize_array(data, min_norm, max_norm):    

    normalizedData = []
    for i in range(data.shape[1]):
        series = Series(data[:,i])
        values = series.values
        values = values.reshape((len(values), 1))
        scaler = MinMaxScaler(feature_range=(min_norm, max_norm))
        scaler = scaler.fit(values)
        normalizedColumn = scaler.transform(values)
        for j in range(normalizedColumn.shape[0]):
            if normalizedColumn[j][0] > max_norm:
                normalizedColumn[j][0] = max_norm
            if normalizedColumn[j][0] < min_norm:
                normalizedColumn[j][0] = min_norm
        if i == 0:
            normalizedData = normalizedColumn
        else:
            normalizedData = np.concatenate((normalizedData, normalizedColumn), axis=1)

    return normalizedData

def normalize_data(data, normalizeOutput, output_indexes_to_remove, output_range_to_remove, min_norm, max_norm):
    
    print("Initializing normalization")

    inputData = data[:,:output_indexes_to_remove]
    outputData = data[:,output_range_to_remove]

    inputData = normalize_array(inputData, min_norm, max_norm)

    if normalizeOutput:
        outputData = normalize_array(outputData, min_norm, max_norm)

    print("Normalization complete")

    return np.concatenate((inputData, outputData), axis=1)

def discrepancy(data, discrepancy_count, output_indexes_to_remove, output_range_to_remove):
    
    print("Initializing discrepancy process")

    inputData = data[:,:output_indexes_to_remove]
    outputData = data[:,output_range_to_remove]
    
    disOut = outputData[1:,:]
    for i in range(2, discrepancy_count + 1):
        disOut = np.concatenate((disOut[:-1,:], outputData[i:,:]), axis=1)

    disIn = outputData[discrepancy_count:,:]
    for i in reversed(range(0, discrepancy_count)):   
        disIn = np.concatenate((outputData[i:-(discrepancy_count - i),:], disIn), axis=1)

    print("Discrepancy process complete")

    return np.concatenate((inputData[discrepancy_count:-discrepancy_count,:], disIn[:-discrepancy_count,:], disOut[discrepancy_count:,:]), axis=1)

def remove_columns_by_correlation(data, outputCount, threshold):

    print("Initializing columns removal by correlation")

    outputRange = np.arange(-outputCount, 0)

    inputData = data[:,:-outputCount]
    outputData = data[:,outputRange]
    
    for oi in range(1,outputCount+1):

        columIndexesToRemove = []

        matrixToCorr = np.concatenate((inputData, outputData[:,:oi]), axis=1)
        corrMatrix = np.corrcoef(matrixToCorr)

        for j in range(0, corrMatrix[-1:,:].shape[1]):
            corr = abs(corrMatrix[-1:,:][0][j])
            if corr < threshold and j not in columIndexesToRemove:
                columIndexesToRemove.append(j)

        for index in sorted(columIndexesToRemove, reverse=True):
            inputData = np.delete(inputData, index, 1)
    
    print(str(len(columIndexesToRemove)) + " column(s) were removed")

    return np.concatenate((inputData, outputData), axis=1)
