print("Starting...")
print("Importing Libraries...")

import numpy as np

import Util as util
import FileManager as fm

INPUT_DATABASES_FOLDER = 'InputDatabases/'
OUTPUT_DATABASES_FOLDER = 'OutputDatabases/'

MIN_NORMALIZATION = 0.15
MAX_NORMALIZATION = 0.85
CORRELATION_THRESHOLD = 0.9

PERCENT_TRAIN = 0.50
PERCENT_CROSS = 0.25
PERCENT_TEST = 0.25

# year and month = 2
IGNORE_FIRST_INDEXES_COUNT = 2
DISCREPANCY_COUNT = 3

SHUFFLE_DATA = True
SPLIT_DATA = True
NORMALIZE_INPUT = True
NORMALIZE_OUTPUT = True
CORRELATION = True

print("-------Global Configuration------")

print("Ignoring the first " + str(IGNORE_FIRST_INDEXES_COUNT) + " column(s)")

print("Remove columns by correlation: " + str(CORRELATION))
if CORRELATION :
    print("Correlation threshold: " + str(CORRELATION_THRESHOLD))

print("Normalize input: " + str(NORMALIZE_INPUT))
if NORMALIZE_INPUT:
    print("Normalize output: " + str(NORMALIZE_OUTPUT))
print("Normalization interval [" + str(MIN_NORMALIZATION) + "," + str(MAX_NORMALIZATION) + "]")

print("Discrepancy count: " + str(DISCREPANCY_COUNT))

print("Shuffle data: " + str(SHUFFLE_DATA))

print("Split data: " + str(SPLIT_DATA))
if SPLIT_DATA:
    print("Percent train: " + str(PERCENT_TRAIN))
    print("Percent cross-validation: " + str(PERCENT_CROSS))
    print("Percent test: " + str(PERCENT_TEST))
    
np.set_printoptions(suppress=True)

results = fm.find_csv_filenames(INPUT_DATABASES_FOLDER)

for filename in results:
    
    print("---------------------------------")
    print("Starting process for " + filename)
    print("---------------------------------")

    old_output_count = 1
    old_output_indexes_to_remove = (-1) * old_output_count
    old_output_range_to_remove = np.arange(old_output_indexes_to_remove, 0)
    
    new_output_count = DISCREPANCY_COUNT
    new_output_indexes_to_remove = (-1) * new_output_count
    new_output_range_to_remove = np.arange(new_output_indexes_to_remove, 0)
    
    data, row_count, col_count = fm.import_database(INPUT_DATABASES_FOLDER + filename)

    if IGNORE_FIRST_INDEXES_COUNT > 0:
        data = data[:,IGNORE_FIRST_INDEXES_COUNT:]
    
    if CORRELATION:
        data = util.remove_columns_by_correlation(data, old_output_count, CORRELATION_THRESHOLD)

    if NORMALIZE_INPUT:
        data = util.normalize_data(data, NORMALIZE_OUTPUT, old_output_indexes_to_remove, old_output_range_to_remove, MIN_NORMALIZATION, MAX_NORMALIZATION)

    if DISCREPANCY_COUNT > 0:
        data = util.discrepancy(data, DISCREPANCY_COUNT, old_output_indexes_to_remove, old_output_range_to_remove)
    
    if SHUFFLE_DATA:
        np.random.shuffle(data)

    row_count = data.shape[0]
    col_count = data.shape[1]

    if SPLIT_DATA:
        train_count = int(np.floor(row_count * PERCENT_TRAIN))
        cross_count = int(np.floor(row_count * PERCENT_CROSS))
        test_count = int(np.floor(row_count * PERCENT_TEST))

        train_start = 0
        cross_start = train_start + train_count
        test_start = cross_start + cross_count

        train_end = train_start + train_count
        cross_end = cross_start + cross_count
        test_end = test_start + test_count

        print("Splitting database")

        data_train = data[np.arange(train_start, train_end), :]
        data_cross = data[np.arange(cross_start, cross_end), :]
        data_test = data[np.arange(test_start, test_end), :]

        input_train = data_train[:,:new_output_indexes_to_remove]
        input_cross = data_cross[:,:new_output_indexes_to_remove]
        input_test = data_test[:,:new_output_indexes_to_remove]

        output_train = data_train[:,new_output_range_to_remove]
        output_cross = data_cross[:,new_output_range_to_remove]
        output_test = data_test[:,new_output_range_to_remove]
    
        print("Splitting complete")
        
        print("Exporting database")

        fm.create_dir(OUTPUT_DATABASES_FOLDER)

        fm.export_to_csv(input_train,  OUTPUT_DATABASES_FOLDER + "input_train_" + filename)
        fm.export_to_csv(input_cross,  OUTPUT_DATABASES_FOLDER + "input_cross_" + filename)
        fm.export_to_csv(input_test,  OUTPUT_DATABASES_FOLDER + "input_test_" + filename)

        fm.export_to_csv(output_train,  OUTPUT_DATABASES_FOLDER + "output_train_" + filename)
        fm.export_to_csv(output_cross,  OUTPUT_DATABASES_FOLDER + "output_cross_" + filename)
        fm.export_to_csv(output_test,  OUTPUT_DATABASES_FOLDER + "output_test_" + filename)
    
        print("Exporting complete")    

    else:
        print("Exporting database")
        fm.create_dir(OUTPUT_DATABASES_FOLDER)
        fm.export_to_csv(data, OUTPUT_DATABASES_FOLDER + "output_" + filename)
        print("Exporting complete")    

    print("Columns count: " + str(col_count))
    print("Input columns count: " + str(col_count - new_output_count))