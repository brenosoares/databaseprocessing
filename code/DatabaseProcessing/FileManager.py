import os
import pandas as pd
import numpy as np

def import_database(filename_path):

    database_path = get_full_path(filename_path)
        
    print("Importing '" + os.path.basename(database_path) + "'")

    # Import data
    data = pd.read_csv(database_path,header=0, sep=';')

    # Dimensions of dataset
    row_count = data.shape[0] # gives number of row count
    col_count = data.shape[1] # gives number of col count

    # Make data a numpy array
    data = data.values

    print("Database imported")

    return data, row_count, col_count

def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def export_to_csv(array, filename):
    a = np.asarray(array)
    np.savetxt(filename, a, delimiter=";", fmt='%s')
    print("Exported to '" + filename + "'")

def find_csv_filenames( dir, suffix=".csv" ):
    path_to_dir = os.path.join(os.getcwd(), dir)
    filenames = os.listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

def get_full_path(filename):
    return os.path.join(os.getcwd(), filename);

